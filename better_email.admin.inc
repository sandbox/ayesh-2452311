<?php

function better_email_admin_settings_form() {
  $form = array();
  
  dpm(variable_get('better_email_status', 1));
  $form['better_email_status'] = array(
    '#title' => t('Enable Better Email functionality.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('better_email_status', 1),
  );
  
  $settings = variable_get('better_email_settings', array());
  
  $form['better_email_settings'] = array(
    '#title' => t('Configuration'),
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#states' => array(
      'visible' => array(
        ':input[name=better_email_status]' => array('checked' => TRUE),
      ),
    ),
    
    'css' => array(
      '#type' => 'textarea',
      '#rows' => 3,
      '#title' => t('Site-wide mail CSS files'),
      '#default_value' => isset($settings['css']) ? $settings['css'] : '', 
      '#description' => t('Enter a list of paths (relative to the Drupal root), 
      that will be included in the CSS compressor. Better Email module will
      include its own basic CSS file. Other modules can implement its own CSS files.')
    ),
  );
  
  
  
  return system_settings_form($form);
}